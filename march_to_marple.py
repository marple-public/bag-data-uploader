import os
import shutil
import tkinter as tk
from tkinter import filedialog
import pandas as pd
import re

from bagpy import bagreader
# pip install marpledata
from marple import Marple

ACCESS_TOKEN = ''
MARPLE_FOLDER = 'api-test'
ALLOWED_TOPICS = ['/march/*']


def main(upload=False, cleanup=False):
    root = tk.Tk()
    root.withdraw()

    m = []
    if upload:
        m = Marple(ACCESS_TOKEN)
        if m.check_connection():
            print('Marple connection OK')
        else:
            print('Could not connect to Marple')
            return

    file_paths = filedialog.askopenfilename(initialdir=os.getcwd(),multiple=True)

    for file_path in file_paths:
        file_name = os.path.basename(file_path)

        # Reading the data
        print(f'Converting {file_name}..')
        bag = bagreader(file_path, verbose=False, tmp=True)
        data_topic = []
        topic_list = get_topic_list(bag)
        for group_id, topic in enumerate(topic_list):
            print(topic)
            temp_data_file = read_topic(bag, topic)
            with open(temp_data_file) as f:
                df = pd.read_csv(f)
            df = clean_dataframe(df)
            df.to_csv(temp_data_file, mode='w+', index=False)
            data_topic.append((topic, temp_data_file))
        print(f'Converting Done!')
        print()

        # new folder name for our csv files
        folder_name = file_name.replace('.bag', '')

        # Clean up old files
        if os.path.exists(folder_name):
            shutil.rmtree(folder_name)
            zip_file_name = f'{folder_name}.zip'
            os.remove(zip_file_name)

        # move all the data around in correct folders
        print(f'Organising data ..')
        os.mkdir(folder_name)
        for (topic, data_path) in data_topic:
            if os.name == 'nt':
                topic_folder = os.path.join(folder_name, topic.strip("/").replace('/', "\\"))
                folders = topic_folder.split("\\")
            else:
                topic_folder = os.path.join(folder_name, topic.strip("/"))
                folders = topic_folder.split("/")
            new_file_name = f'{folders[-1]}.csv'
            new_folder = os.path.join(*folders[:-1])
            os.makedirs(new_folder, exist_ok=True)
            shutil.move(data_path, os.path.join(new_folder, new_file_name))
        zip_file_name = shutil.make_archive(folder_name, 'zip', folder_name)

        # Upload data to Marple
        if upload:
            print(f'Uploading {file_name}..')
            m.upload_data_file(zip_file_name, MARPLE_FOLDER, metadata={}, plugin='csv_zip_plugin')
            print('Uploading complete!')
            print(f'Cleaning up..')

        # Cleanup all the data created
        if cleanup:
            shutil.rmtree(folder_name)
            os.remove(zip_file_name)
    print('Done!')


def read_topic(bag, topic):
    tempfile = f'{bag.datafolder}/{topic[1:].replace("/", "-")}.csv'
    return tempfile if os.path.exists(tempfile) else bag.message_by_topic(topic)


def get_topic_list(reader):
    if len(ALLOWED_TOPICS) == 0:
        return reader.topics

    topic_list = []
    for topic in reader.topics:
        if any(re.match(atopic, topic) for atopic in ALLOWED_TOPICS):
            topic_list.append(topic)
    return topic_list


def clean_dataframe(data_frame):
    # Remove duplicate time
    data_frame['Time'] = data_frame['Time'].round(3)
    data_frame.drop_duplicates(subset=['Time'], inplace=True)

    for header in data_frame:
        if data_frame[header].dtype == 'object':
            try:
                new_df = string_to_float(data_frame[header])
                for (i, new_header) in enumerate(new_df):
                    data_frame[f'{header}_{i}'] = new_df[new_header]
                del data_frame[header]
            except ValueError as e:
                pass

    # remove unnamed columns
    data_frame = data_frame.loc[:, ~data_frame.columns.str.contains('^Unnamed')]
    return data_frame


def string_to_float(data_frame: pd.DataFrame) -> pd.DataFrame:
    data_frame = data_frame.str[1:-1]  # Remove brackets from string
    data_frame = data_frame.str.split(",", expand=True)  # Split string to individual joints
    data_frame = data_frame.astype('float')  # Convert string to float
    return data_frame


if __name__ == '__main__':
    main(upload=True, cleanup=True)
